# Description
This is a test space for demonstrating a failure that Sam has not been able to solve 
when using ATLAS on Condor via ReAna submission.

# Contents
There are four different workflows that all run the same analysis that just makes a histogram and saves a PDF and root file of it.  This is meant to mimic an analysis selection stage that will need to be run over many files and therefore necessitate the use of HTCondor.  Two are based on non-ATLAS images and two are based on ATLAS images.  

## workflow_simple_noatlas
  - Based on the analysis image [https://gitlab.cern.ch/workflows/analysisbare](https://gitlab.cern.ch/workflows/analysisbare) which ultimately derives from `rootproject/root-conda:6.18.04` 
  - Runs on `kubernetes` backend

## workflow_simple_noatlas_condor
  - Based on the analysis image [https://gitlab.cern.ch/workflows/analysisbare](https://gitlab.cern.ch/workflows/analysisbare) which ultimately derives from `rootproject/root-conda:6.18.04` 
  - Runs on `htcondorcern` backend

## workflow_simple_withatlas
  - Based on the analysis image [https://gitlab.cern.ch/workflows/reanaworkshop](https://gitlab.cern.ch/workflows/reanaworkshop) which ultimately derives from `atlas/analysisbase:21.2.51` 
  - Runs on `kubernetes` backend
  
## workflow_simple_withatlas_condor
  - Based on the analysis image [https://gitlab.cern.ch/workflows/reanaworkshop](https://gitlab.cern.ch/workflows/reanaworkshop) which ultimately derives from `atlas/analysisbase:21.2.51` 
  - Runs on `htcondorcern` backend
  
  
# Running
I have been running stuff on the `https://reana-qa.cern.ch/` installation of ReAna
  
  
# Issues
There are two primary issues.

## ATLAS Won't Run
The primary issue is that the `workflow_simple_withatlas_condor` fails with this error
```
Job logs of 8012909 were not found. [Errno 2] No such file or directory: '/var/reana/users/3469d90e-04c4-496f-850c-a31e6d452879/workflows/bd663b22-3507-47b7-ac3a-3b2db1b0ca8e/reana_job.8012909.0.err'
```

**REQUEST** : Help me find out what is causing this when we run the `/home/atlas/release_setup.sh` in the image (which is necessary for ATLAS SW).

You can examine this file im the image by doing
```
docker run --rm -it gitlab-registry.cern.ch/workflows/reanaworkshop:master
cat /home/atlas/release_setup.sh 
cat /usr/AnalysisBase/*/InstallArea/*/setup.sh
```





# Additional Thoughts
  - **Open Up reana.cern.ch** (Not HTCondor Focused): Make the interface open so you don't need `sshuttle`.  That small barrier reduction will greatly increase adoption by "the masses"
  - **virtualenv** (Not HTCondor Focused): The second "barrier" to improve would be having to work within a virtualenv.  Can things not be simplified to be more akin to `recast` which you `pip install` it and are ready to go?  Again, small barriers with new tools add friction to adoption.
  - **Documentation** (Not HTCondor Focused): We have found success with the HSF Training model of [SWC-like tutorial](https://hsf-training.github.io/hsf-training-cicd/) *and additional videos* that [recreate the in-person experience](https://www.youtube.com/watch?list=PLKZ9c4ONm-VmmTObyNWpz4hB3Hgx8ZWSb&v=NcVGX8zWzQY).  We are currently making such a setup for a [RECAST tutorial](http://sammeehan.com/standalone-recast-tutorial/), and I think with slight modifications/extensions, this can serve such a role for ReAna.
  - **Log Files**: I can't find my HTCondor log files in [the workspace](https://reana-qa.cern.ch/details/bd663b22-3507-47b7-ac3a-3b2db1b0ca8e), so I am having a really hard time debugging.  For example, for the job above, this file `/var/reana/users/3469d90e-04c4-496f-850c-a31e6d452879/workflows/bd663b22-3507-47b7-ac3a-3b2db1b0ca8e/reana_job.8012909.0.err` should be accessible in the online gui since that it the primary portal for ReAna users.  I don't want to have to navigate through another set of resources.
  - **Grid Access**: With Clemens help, I could (now its broken) access the grid [in a docker image](https://gitlab.cern.ch/workflows/reanaworkshop/-/blob/master/Dockerfile#L13).  Maybe this is not revolutionary, but it sure isn't well documented anywhere and will be essential for large scale running.  This was hacked together and was potentially not secure in terms of how I imported my grid certificate as one long string to the job as an input parameter (ahhhhhhhhhh!).  This should be standardized. 
  - **Standard Job "Modules"**: There will be standard things that all analyzers need.  We just need to reflect on what such functionality exists on the grid and repurpose or reimplement that.
    - Retrieval/organization of input data.  Basically file-list creation.  It would seem to me that this should be grid-integrated with the functionality of performing a grid transfer request to the nearest grid endpoint before submitting worker jobs that run on this data.  The reasoning here is that the HTCondor resources that you are running on is fixed, but the data may be anywhere.  This differs from the grid and would make HTCondor prohibitive if you are streaming data from far away.
    - Gathering of job outputs.  Why does everyone need to write an `hadd` job.  Can't a generic "intelligent" gathering job be created?
  - **Job Retries**: HTCondor DAGMan has an [RETRY](https://research.cs.wisc.edu/htcondor/manual/v7.6/2_10DAGMan_Applications.html#SECTION003106100000000000000) feature.  
  - **Development Time**: The second issue is that it takes hella long for the HTCondor jobs to get submitted.  This issue was found to persist when trying to use the `espresso` settings for job queue timing.  This is not so much an "insurmountable" issue, but it makes it practically more challenging to desire adopting this mode of ReAna for large scale running.  It also makes it very challenging to develop/debug workflows sent to reana.  **REQUEST** : Can we have some sort of "scout"-like or "dev" job setting that gets high priority, but saves no actual output (or has some other mechanism to prevent abuse)?

